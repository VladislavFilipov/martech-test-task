import React from 'react';
import About from '../About/About';
import Head from '../Head/Head';
import './Card.scss';

function Card() {
    return (
        <div className="Card">
            <Head />
            <About />
        </div>
    );
}

export default Card;
