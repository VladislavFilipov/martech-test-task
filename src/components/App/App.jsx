import React, { useEffect, useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import Card from '../Card/Card';
import './App.scss';

function App() {
    const [showCard, setShowCard] = useState(false);

    useEffect(() => {
        setShowCard(true);
    }, []);

    return (
        <div className="App">
            <CSSTransition
                in={showCard}
                timeout={1000}
                classNames="an-card"
                unmountOnExit
            >
                <Card />
            </CSSTransition>
        </div>
    );
}

export default App;
