import React, { useEffect, useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import './About.scss';

function About() {
    const [showAbout, setShowAbout] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            setShowAbout(true);
        }, 1000);
    }, []);

    return (

        <div className="About">
            <CSSTransition
                in={showAbout}
                timeout={500}
                classNames="an-about"
                unmountOnExit
            >
                <div>
                    <div className="title">Saba</div>
                    <div className="subtitle">Band / Musician</div>
                    <p className="info">
                        PIVOTGANG  <span role="img" aria-label="">🏀</span><br />
                        CARE FOR ME TOUR OUT NOW <span role="img" aria-label="">🎙</span><br />
                        <a target="_blank" rel="noopener noreferrer" href="https://www.google.com/maps/place/%D0%A7%D0%B8%D0%BA%D0%B0%D0%B3%D0%BE,+%D0%98%D0%BB%D0%BB%D0%B8%D0%BD%D0%BE%D0%B9%D1%81,+%D0%A1%D0%A8%D0%90/@41.8333925,-88.0121478,10z/data=!3m1!4b1!4m5!3m4!1s0x880e2c3cd0f4cbed:0xafe0a6ad09c0c000!8m2!3d41.8781136!4d-87.6297982">#CHI-TOWN</a> <br />
                        This remind me of before we had insomnia<br />
                        Sleepin' peacefully, never needed a pile of drugs
                    </p>
                    <a href="http://pivotgang.com" rel="noopener noreferrer" target="_blank" className="site">pivotgang.com</a>
                </div>
            </CSSTransition>
        </div>


    );
}

export default About;
