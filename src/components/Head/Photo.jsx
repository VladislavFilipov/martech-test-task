import React from 'react';

function Photo() {
    return (
        <div className="Photo">
            <img src="images/user.png" alt="" />
        </div>
    );
}

export default Photo;
