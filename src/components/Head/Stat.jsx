import React from 'react';

function Stat() {
    return (
        <div className="Stat">
            <div className="item">
                <div className="num">142</div>
                <div className="text">Posts</div>
            </div>
            <div className="item">
                <div className="num">7.4M</div>
                <div className="text">Followers</div>
            </div>
            <div className="item">
                <div className="num">117</div>
                <div className="text">Following</div>
            </div>
        </div>
    );
}

export default Stat;
