import React, { useEffect, useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import Actions from './Actions';
import './Head.scss';
import Photo from './Photo';
import Stat from './Stat';

function Head() {
    const [showPhoto, setShowPhoto] = useState(false);
    const [showStat, setShowStat] = useState(false);
    const [showActions, setShowActions] = useState(false);
    // const nodeRef = useRef(null);

    useEffect(() => {
        setTimeout(() => {
            setShowPhoto(true);
            setShowStat(true);
            setShowActions(true);
        }, 1000);

    }, []);

    return (
        <div className="Head">
            <CSSTransition
                in={showPhoto}
                timeout={500}
                classNames="an-photo"
                unmountOnExit
            >

                <Photo />
            </CSSTransition>
            <CSSTransition
                in={showStat}
                timeout={500}
                classNames="an-stat"
                unmountOnExit
            >
                <Stat />
            </CSSTransition>
            <CSSTransition
                in={showActions}
                timeout={500}
                classNames="an-actions"
                unmountOnExit
            >
                <Actions />
            </CSSTransition>
        </div >
    );
}

export default Head;
