import React, { useState } from 'react';

import { CSSTransition } from 'react-transition-group';

function Actions() {
    const [showMessage, setShowMessage] = useState(false);
    const [followed, setFollowed] = useState(true);


    return (
        <div className="Actions">
            <button
                className="show-message"
                onClick={() => setShowMessage(true)}
            >Message</button>
            <button
                className={`follow ${followed ? '' : 'unfollowed'}`}
                onClick={() => setFollowed(!followed)}
            >
                <img src="images/follow.png" alt="" />
            </button>
            <CSSTransition
                in={showMessage}
                timeout={500}
                classNames="an-message"
                unmountOnExit
            >
                <div className="message">
                    <span>It's arbitrary message with arbitrary design<span role="img" aria-label="">🌚</span></span>
                    <div
                        className="close"
                        onClick={() => setShowMessage(false)}
                    >+</div>
                </div>
            </CSSTransition>
        </div>
    );
}

export default Actions;
